import React, { Component } from 'react';
import A from './A.jsx';
import B from './B.jsx';
import G from './G.jsx';
import Select from 'react-select';
const techCompanies = [
  { label: "Apple", value: 1 },
  { label: "Facebook", value: 2 },
  { label: "Google", value: 3 }
  
];
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectValue:'',
            displayPage:''
          
        }
        this.handleChange = this.handleChange.bind(this);
        
        this.showPage = null;
        
    }
    exampleSwitch(key)
    {
        switch(key)
        {
          case 'Apple' : 
          
          this.showPage= <A /> 
          this.setState({displayPage:this.showPage}) 
            break;
          case 'Facebook' :
          this.showPage= <B />
          this.setState({displayPage:this.showPage})
          break;
          case 'Google' :
          this.showPage= <G />
          this.setState({displayPage:this.showPage})
          break;  
            
        }
    }
    handleChange(e){
        this.setState({selectValue:e.label})
      }

   render() {
    
      return (
          
        <div>
          <Select options={ techCompanies } 
          onChange={this.handleChange}/>
      <button type="submit" onClick={() => { this.exampleSwitch(this.state.selectValue) }}>Proceed</button>
       {this.showPage}
       </div>
        
         
      );
   }
}
export default App;